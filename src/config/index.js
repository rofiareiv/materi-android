import firebase from 'firebase';

firebase.firebaseConfig = {
  apiKey: 'AIzaSyAzs7fC4mI3_LWHrwOzgAUSVr1aUjZgg_M',
  authDomain: 'materi-8b68e.firebaseapp.com',
  projectId: 'materi-8b68e',
  storageBucket: 'materi-8b68e.appspot.com',
  messagingSenderId: '614668630611',
  appId: '1:614668630611:web:966478ca383dfef29be4a6',
  measurementId: 'G-MC6CWQCEXW',
};

const api = firebase;

export default api;
