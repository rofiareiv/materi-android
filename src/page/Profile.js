import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      profil: this.props.route.params.name,
    };
  }

  render() {
    return (
      <View>
        <Text> Nama {this.state.profil.nama}</Text>
        <Text> Alamat {this.state.profil.alamat}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
