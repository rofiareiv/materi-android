import React, {Component} from 'react';
import {Alert, Text, TextInput, View, StyleSheet} from 'react-native';
import CustomButton from '../component/CustomButton';

export class Home extends Component {
  state = {
    name: 'Kambing',
    alamat: '',
    profil: {
      nama: 'Ayu',
      alamat: 'Blumbungan',
    },
  };
  render() {
    return (
      <View>
        <Text style={{fontSize: 24}}> contoh aja </Text>
        <TextInput
          onChangeText={text => this.setState({name: text})}
          style={styles.input}
        />
        <TextInput
          onChangeText={text => this.setState({alamat: text})}
          style={styles.input}
        />
        <Text style={styles.teks}>Nama : {this.state.name}</Text>
        <Text style={styles.teks}>Alamat : {this.state.alamat}</Text>
        <CustomButton
          label="Ke Hal About"
          onPress={() => this.props.navigation.navigate('About')}
        />
        <CustomButton
          label="Ke Hal Profile"
          onPress={() =>
            this.props.navigation.navigate('Profile', {name: this.state.profil})
          }
        />
      </View>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderColor: 'green',
    margin: 5,
    padding: 10,
  },
  teks: {
    fontSize: 18,
    color: 'blue',
    margin: 5,
  },
});
